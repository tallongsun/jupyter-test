## conda env setup
```
conda remove --name opencv36 --all
conda create --name opencv36 python=3.6
conda activate opencv36
conda install opencv
conda install -c conda-forge imutils
conda install -c gilbertfrancois dlib    
brew install libpng
conda install jupyter notebook
jupyter notebook
conda deactivate
```

## jupyter notebook config
cat ~/.jupyter/jupyter_notebook_config.py |grep notebook_dir
> c.NotebookApp.notebook_dir = '/Users/xxx/jupyter'
